import { useEffect, useState } from "react";
import { Redirect } from "react-router";
import { Link } from "react-router-dom";
import { API } from "../../Shared/Services/Api";
import { getCookieUtil } from "../Utils/getCookieUtil";
import "./GoogleMap.scss"

var inputChuli="";



export function GoogleMap() {
  const[arrayAllMyGuardians,setarrayAllMyGuardians] = useState([]);

  let myGuardians =[];
  const allGuardians = guardiansList => {
    API.get('allposts',guardiansList).then(res =>{
      myGuardians = res.data.results
      console.log("CARGA DE RESULTADOS")
      console.log(myGuardians)
      setarrayAllMyGuardians(myGuardians)
    })
    // loadmapa()
  }

  function loadmapa(){
    console.log("CARGA DEL MAPA")
    const map = new window.google.maps.Map(document.getElementById("map"), {
      center: { lat: 40.4165000, lng: -3.7025600 },
      zoom: 13 
  });
  
  // Add a marker clusterer to manage the markers.
  console.log(myGuardians)
  const clustermarkers = arrayAllMyGuardians.map((guardian, i) => {
        console.log("Nombre del Guardian:")
        console.log(guardian.user.name)
        console.log("LATITUD del Guardian:")
        console.log(guardian.latPosition)
        console.log("LONGITUD del Guardian:")
        console.log(guardian.lngPosition)
        // var location = [{lat: parseFloat(asd.latPosition), lng: parseFloat(asd.lngPosition)}]
        // console.log(location)
    const marker = new window.google.maps.Marker({
      
      position : {
        lat : parseFloat(guardian.latPosition),
        lng : parseFloat(guardian.lngPosition),
    },
      label: guardian.user.name
    });
    marker.addListener("click", () => {
      
      console.log("HOLA")
      console.log(guardian._id)
      // window.location.replace("http://localhost:3000/userhome/"+token+"/"+userId);
      window.location.replace("http://localhost:3000/postdetails/"+guardian._id);
      


    })
    return marker
  });
  // Add a marker clusterer to manage the markers.
  new window.MarkerClusterer(map, clustermarkers, {
    imagePath:
      "https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m",
  })
  



  //   map.addListener("click", () => {
  //   console.log("nosotros somos los putos amos si venis por aqui sus rajamos")
  // });


  const input = document.getElementById("pac-input");
  
  const searchBox = new window.google.maps.places.SearchBox(input);
  map.controls[window.google.maps.ControlPosition.TOP_LEFT].push(input);
  console.log("miinbox")
  map.addListener("bounds_changed", () => {
      searchBox.setBounds(map.getBounds());
    });
    let markers = [];
    searchBox.addListener("places_changed", () => {
      const places = searchBox.getPlaces();
  
      if (places.length === 0) {
        return;
      }
      // Clear out the old markers.
      markers.forEach((marker) => {
        marker.setMap(null);
      });
      markers = [];
      // For each place, get the icon, name and location.
      const bounds = new window.google.maps.LatLngBounds();
      places.forEach((place) => {
        if (!place.geometry || !place.geometry.location) {
          console.log("Returned place contains no geometry");
          return;
        }
        // console.log(place.geometry)
        console.log(place.geometry.location.lat())
        console.log(place.geometry.location.lng())
        // console.log("Aqui tenemos la latitud"+ place.geometry.viewport)

        const icon = {
          url: place.icon,
          size: new window.google.maps.Size(71, 71),
          origin: new window.google.maps.Point(0, 0),
          anchor: new window.google.maps.Point(17, 34),
          scaledSize: new window.google.maps.Size(25, 25),
        };
        // Create a marker for each place.
        markers.push(
          new window.google.maps.Marker({
            map,
            icon,
            title: place.name,
            position: place.geometry.location,
          })
        );
  
        if (place.geometry.viewport) {
          // Only geocodes have viewport.
          bounds.union(place.geometry.viewport);
        } else {
          bounds.extend(place.geometry.location);
        }
      });
      map.fitBounds(bounds);
    });  
  }
  
  
    useEffect(allGuardians,[]);
    useEffect(loadmapa,[myGuardians]);

    

    const mapStyleS = {height: '90vh'}
   

    const valueSearchBox = () => {
      inputChuli = document.getElementById("pac-input").value;
      console.log(inputChuli)
    }
    

    return (
      <div>
      {/* <input type="submit" onClick={valueSearchBox}/> */}
          <div className="App">
              <div style={mapStyleS} id="map"/>
               <input
                  id="pac-input"
                  className="controls"
                  type="text"
                  placeholder="Search Box"
                  />

          </div>
         
      </div>
    );
}