
import React, { useEffect, useState } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import './App.css';
import { Routes } from './Core/Routes/Routes';
import { IsLoggedContext, loggedAsContext, tokenValue } from './Shared/Contexts/IsLoggedContext';
import { getCookieUtil } from './Shared/Utils/getCookieUtil';
import { CoordenadasContext } from './Shared/Contexts/Coordenadas';
import { Navbar } from './Core/Navbar/Navbar';
import { GuardianDetailsChatContext } from './Shared/Contexts/GuardianDetailsChatContext';
import { API } from './Shared/Services/Api';





function App() {

  // const history = useHistory();
  let cookieToken = true;

  const [loggedAs, setLoggedAs] = useState('user');
  const [isLogged, setIsLogged] = useState(false);
  const [coors, setCoors] = useState({});
  const [direction, setDirection] = useState();
  const [ tokenValuee, setTokenValuee ] = useState();
  const [guardianDetailsChatContext, setGuardianDetailsChatContext] = useState()
  // const [tokenSet, setTokenSet] = useState("");
  


    function initApp(){
        const value =  getCookieUtil('loggedAs');
        setLoggedAs(value)
    }



    useEffect(initApp,[getCookieUtil('loggedAs')])

  function verifyToken(){
    // console.log(tokenSet);
    setLoggedAs(getCookieUtil('loggedAs'))
    setIsLogged(!!getCookieUtil('isLogged'))
    // API.post('init').then(res => {
    //     document.cookie ='isLogged= true';
    //     setIsLogged(true)
    //     const loggedAs2 = getCookieUtil('loggedAs');
    //     setLoggedAs(loggedAs2)
    //   }).catch( err =>{
    //     setIsLogged(false)
    //     document.cookie = 'isLogged= false';
    // });
}

  useEffect(verifyToken,[]);

  return (
<>
    

  <tokenValue.Provider value ={{ tokenValuee, setTokenValuee }}>
  <IsLoggedContext.Provider value ={{isLogged, setIsLogged}}>
  <loggedAsContext.Provider value ={{loggedAs, setLoggedAs}}>
  <CoordenadasContext.Provider value ={{coors,setCoors}}>
  <GuardianDetailsChatContext.Provider value ={{guardianDetailsChatContext, setGuardianDetailsChatContext}}>
      
      {/* <Socketio/>  */}
      <Router>
        <div className="router__link">
          
          <Routes/>
          
        </div>
        <Navbar/>
      </Router>
    </GuardianDetailsChatContext.Provider>
    </CoordenadasContext.Provider>
    </loggedAsContext.Provider>
    </IsLoggedContext.Provider>
    </tokenValue.Provider>
  
  </>
  );
}

export default App;
