import { Navbar } from "../../../Core/Navbar/Navbar";
import { useContext, useEffect, useRef, useState} from 'react';
import { Calendar } from 'primereact/calendar';
import './HomeUser.scss';
import searchOrange from '../../../Assets/Logos/lupaOrange.png';
import Swiper from 'swiper';
import SwiperCore, { Navigation, Pagination } from 'swiper/core';
import 'swiper/swiper-bundle.css';
import { useHistory, useParams } from "react-router";
import { API } from "../../../Shared/Services/Api";
import img1 from '../../../Assets/img/img1.png';
import img2 from '../../../Assets/img/img2.png';
import img3 from '../../../Assets/img/img3.png';
import img4 from '../../../Assets/img/imgCasa1.jpeg';
import img5 from '../../../Assets/img/imgCasa2.jpeg';
import { IsLoggedContext, loggedAsContext, tokenValue } from "../../../Shared/Contexts/IsLoggedContext";
import socketIOClient from "socket.io-client";
import { getCookieUtil } from "../../../Shared/Utils/getCookieUtil";


const ENDPOINT = "http://127.0.0.1:5050";
  // "https://cors-anywhere.herokuapp.com/"+
  
export const APIHeaders = {
    // 'Accept': 'application/json',
    'Accept': '*/*',
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    
};

SwiperCore.use([Navigation, Pagination]);

var location="";
var numberOfBaggage =0;

export function HomeUser(){
    // const {token} = useParams();
    const { isLogged, setIsLogged } = useContext(IsLoggedContext);
    const { loggedAs, setLoggedAs } = useContext(loggedAsContext);
    const { tokenValuee, setTokenValuee } = useContext(tokenValue);
    const [ receiveToken, setReceiveToken ] = useState(false);
    const history = useHistory();
    const socket = socketIOClient(ENDPOINT,{
        withCredentials: true,
    });
    const moment = new Date();
    const swipeRef = useRef(null);

    const [dateDeposit, setDateDeposit] = useState(new Date());
    const [dateCollect, setDateCollect] = useState(new Date());


    function sendValues(){

        const valueDeposit = dateDeposit.toString().split(" ",5)
        const month = monthToNumber(valueDeposit[1]);
        const depositDate =  valueDeposit[3] + "/" + month +"/"+valueDeposit[2];
        const valueCollect = dateCollect.toString().split(" ",5)
        const collectDate =  valueCollect[3] + "/" + month +"/"+valueCollect[2];

        location = document.getElementById("location").value;
        numberOfBaggage = document.getElementById("numberBaggage").value;
        console.log("Fecha de deposito");   console.log(depositDate);
        console.log("Fecha de Recogida");   console.log(collectDate);
        console.log("Localizacion");        console.log(location); document.cookie = 'location=' + location;
        console.log("Numero de maletas");   console.log(numberOfBaggage);

        document.cookie = 'arrive=' + depositDate;
        document.cookie = 'collect=' + collectDate;
        document.cookie = 'baggage=' + numberOfBaggage;
        // if()
        history.push('/searchGuardian')
    }

    const img = [img3, img2, img1];

    const createSlider =  ()=>{
        const swiperContainer$$ = swipeRef.current;
        // const swiperContainer$$ = document.querySelector('.swiper-container');
        
    new Swiper(swiperContainer$$, {
        // Optional parameters
        direction: 'horizontal',
        slidesPerView: 'auto',
        spaceBetween: 30,

        pagination: {
          el: '.swiper-pagination',
          type: 'bullets',
          clickable: true,
        },      
      });
    }

    useEffect(createSlider, [])

    function monthToNumber(date){
        let value;
        console.log("El date que entra:"+date);
        if( date == "Jan"){ value = "1"}
        else if( date == "Feb"){ value = "2"}
        else if( date == "Mar"){ value = "3"}
        else if( date == "Apr"){ value = "4"}
        else if( date == "May"){ value = "5"}
        else if( date == "Jun"){ value = "6"}
        else if( date == "Jul"){ value = "7"}
        else if( date == "Aug"){ value = "8"}
        else if( date == "Sep"){ value = "9"}
        else if( date == "Oct"){  value = "10"}
        else if( date == "Nov"){  value = "11"}
        else if( date == "Dec"){  value = "12"}
        
        return value
    }


    // function SetSocket () {
    //     socket.emit("GoogleOrFacebookTokenRequestHome",'sdd');
    //     GetSocket()
    // }

    // function GetSocket(){
    //     console.log('Entro a getSocket');
    //     socket.on("GoogleOrFacebookTokenRequestHome", response => {

    //         console.log('Google Face');

    //         console.log('response',response);
    //         console.log('responseToken',response.token);
    //         // document.cookie = 'token=' + response;
    //         // document.cookie ='token ='+response.token;
    //         setTokenValuee(response)
    //         console.log(tokenValuee);
    //         setReceiveToken(true)
  
    const [ showmore, setShowMore ]= useState(false)
    function showMore(){
        if(showmore == false){
            setShowMore(true)
        }else{
            setShowMore(false)
        }
    }

    
    

    function verifyToken(){
        // if(receiveToken){
        // console.log( getCookieUtil('token'));
        API.post('init').then(res => {
            document.cookie ='isLogged= true';
            document.cookie ='loggedAs= user';
            setIsLogged(true)
        }).catch( err =>{
            document.cookie = 'isLogged= false';
            setIsLogged(false)
            // history.push('login')
            });

        // if(getCookieUtil('sLogged')==false && tokenValuee.token !=undefined){
        //     history.push('login')
        //     // }
        // }
        // if(tokenValuee ){
        //     console.log('token Value');
        //     console.log(tokenValuee.token);
        //     const token3 = tokenValuee.token;
        //     console.log('token3: '+token3);
        //     document.cookie ='token ='+ token3;
        // }
    }
    
    useEffect(verifyToken, []);
    // useEffect( SetSocket , [] );
    // useEffect( GetSocket , [] );


    return(
    <div className="homeUserCenter router__link">
     <div className="userHome">
            <h1 className="userHome__title">Encuentra tu guardián</h1>
        <div className="userHome__search">
            <img className="userHome__searchLogo" src={searchOrange} alt=""/>
            <input className="userHome__searchInput" type="text" id="location" placeholder="¿Dónde te encuentras? Madrid, Barcelona..." />
        </div>

        <div className="userHome__details">
            <div className="userHome__calendarOne">
                <h3 className="userHome__h3">Depósito</h3>
                <Calendar minDate = {moment} className="userHome__input" dateFormat="dd/mm/yy" value={dateDeposit} onChange={(e) => setDateDeposit(e.value)} showTime  /> 
                
            </div>
            <div className="userHome__calendarTwo">
                <h3 className="userHome__h3">Retirada</h3>
                
                <Calendar minDate = {dateDeposit} className="userHome__input" dateFormat="dd/mm/yy" value={dateCollect}  onChange={(e) => setDateCollect(e.value)} showTime /> 
            </div>

        </div>

        <div className="userHome__baggage">
            <div className="userHome__calendarThree">
            <p className="userHome__h3">Numero de maletas?</p>
                <select className="userHome__numbers" id="numberBaggage">
                    {/* <option value="DEFAULT">0</option> */}
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>
            </div>
            <div>
                <button className="userHome__btn" onClick={sendValues}> Buscar </button>
            </div>
        </div>   

        <div className="news">
            <h2 className="experiences__h2">Novedades</h2>

            <div ref={swipeRef} class="swiper-container">
  
                <div class="swiper-wrapper">
    
                {img.slice(0,3).map((img, i ) => <div class="swiper-slide"><img className="imagenes" src={img} alt="" />
            </div>)}
    
                </div>
 
                <div class="swiper-pagination"></div>

  
                {/* <div class="swiper-button-prev"></div>
                <div class="swiper-button-next"></div> */}

  
               
            </div>




        </div>
        <div className="experiences">
            <div>
                <h2 className="experiences__h2">Experiencias</h2>
                <img className="experiences__img" src={img1} alt="" />
                <p className="experiences__title">Un pedazito de Italia en Gijón</p>
                <p className="experiences__info">Sin lugar a duda es uno de los destinos gastronómicos  por execélncia de esta gran ciudad situada en el norte de España.</p>
            </div>
            <div>
                <img className="experiences__img" src={img2} alt="" />
                <p className="experiences__title">Sumérgete en Barcelona  </p>
                <p className="experiences__info">Si estás dispuesto a aventurarte en el mundo del submarinismo y venir a sentir una vivencia insólita en L’Aquàrium.</p>
            </div>

        </div>
        {}
        <div className={showmore == false ? "experiencesShowMore" : "experiences"}>
            <div>
                <h2 className="experiences__h2">Alquileres</h2>
                <img className="experiences__img" src={img4} alt="" />
                <p className="experiences__title">Casa Tradicional</p>
                <p className="experiences__info">Si quieres pasar un fin de semana recordando las influencias mas tradicionales, no dudes en alquilar esta casa y disfrutar con la familia.</p>
            </div>
            <div>
                <img className="experiences__img" src={img5} alt="" />
                <p className="experiences__title">Casas de ensueño </p>
                <p className="experiences__info">Observa nuestra lista de casas al alcance de muy pocos, en entornos rurales y cosmopolitas. Se pueden visitar por fuera, o si tu bolsillo lo permite, pasar unos días en ellas.</p>
            </div>

        </div>
        <div className="showMore">
            <button className="showMore__btn" onClick={()=> showMore()}>Mostrar más</button>
        </div>



        {/* onClick={()=> showMore()} */}

        {/* <div className={showmore == 0 && }>
        <div className="experiencesShowMore">
            <div>
                <h2 className="experiences__h2">Experiencias</h2>
                <img className="experiences__img" src={img1} alt="" />
                <p className="experiences__title">Un pedazito de Italia en Gijón</p>
                <p className="experiences__info">Sin lugar a duda es uno de los destinos gastronómicos  por execélncia de esta gran ciudad situada en el norte de España.</p>
            </div>
            <div>
                <img className="experiences__img" src={img2} alt="" />
                <p className="experiences__title">Sumérgete en Barcelona  </p>
                <p className="experiences__info">Si estás dispuesto a aventurarte en el mundo del submarinismo y venir a sentir una vivencia insólita en L’Aquàrium.</p>
            </div>

        </div> */}

        
        
    </div>
    
    </div>
    
    

    )
}
