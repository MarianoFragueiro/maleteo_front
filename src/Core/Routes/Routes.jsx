import{ Route, Switch } from "react-router-dom";
import { GetToken } from "../../Pages/All/GetToken/GetToken";
import { MessagesPage } from "../../Pages/All/MessagesPage/MessagesPage";
import { LoginPage } from '../../Pages/All/Register_Login/LoginPage/LoginPage';



import { SeleccionaUbicacion } from "../../Pages/Guardian/LocalizeCoors/SeleccionaUbicacion";
import { WelcomePage } from "../../Pages/All/WelcomePages/WelcomeOne/WelcomePage";
import { BecomeGuardian } from "../../Pages/Guardian/BecomeGuardian/BecomeGuardian";
import { BookingRequest } from "../../Pages/Guardian/BookingRequest/BookingRequest";
import { PostDetails } from "../../Pages/Guardian/PostDetails/PostDetails";
import { PostGuardian } from "../../Pages/Guardian/PostGuardian/PostGuardian";
import { EditUser } from "../../Pages/User/EditUser/EditUser";
import { HomeUser } from "../../Pages/User/HomeUser/HomeUser";
import { InboxPage } from "../../Pages/All/InboxPage/InboxPage";
import { SearchGuardian } from "../../Pages/User/SearchGuardian/SearchGuardian";
import { UserPage } from "../../Pages/All/UserPage/UserPage";
import { BookingDetails } from "../../Pages/Guardian/BookingDetails/BookingDetails";
import { MessagesPage2 } from "../../Pages/All/MessagesPage/MessagePage2";
import { GetTokenGoogleFace } from "../../Pages/All/GetToken/GetTokenGoogleFace";



export function Routes(){
    return(
        <Switch>
                <Route exact path="/">
                    <WelcomePage/>
                </Route>
                
                <Route exact path="/login">
                    <LoginPage/>
                </Route>

                <Route exact path="/bookingrequest">
                    <BookingRequest/>
                </Route>
                
                <Route exact path="/messagespage/:messagesId">
                    <MessagesPage/>
                </Route>

                <Route exact path="/messagespage2/:messagesId">
                    <MessagesPage2/>
                </Route>
               
                {/* NAVBAR */}
                <Route exact path="/userhome">
                    <HomeUser/>
                </Route>

                <Route exact path="/gettoken/:token">
                    <GetToken/>
                </Route>

                <Route exact path="/gettokengf/:token">
                    <GetTokenGoogleFace/>
                </Route>
                <Route path="/searchGuardian">
                    <SearchGuardian/>
                </Route>
                
                <Route path="/becomeGuardian">
                    <BecomeGuardian/>
                </Route>

                <Route path="/inbox">
                    <InboxPage/>
                </Route>
                <Route path="/edituser">
                    <EditUser/>
                </Route>
                <Route path="/userpage">
                    <UserPage/>
                </Route>
                <Route path="/postguardian">
                    <PostGuardian/>
                </Route>
                {/* Detalles de guardian */}
                <Route path="/postdetails/:postId">
                    <PostDetails/>
                </Route>
                <Route exact path="/seleccionaubicacion">
                    <SeleccionaUbicacion/>
                </Route>
                <Route exact path="/bookingdetails">
                    <BookingDetails/>
                </Route>
                <Route exact path="/bookingrequest">
                    <BookingRequest/>
                </Route>
        </Switch>
    )
}