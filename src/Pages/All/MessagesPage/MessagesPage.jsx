

import logoArrow from '../../../Assets/Logos/arrow.png';
import noImage from '../../../Assets/img/No_Image.jpg';
import {  useContext, useEffect, useState } from 'react';
import { API } from '../../../Shared/Services/Api';
import { useHistory } from "react-router";
import { getCookieUtil } from '../../../Shared/Utils/getCookieUtil';
import socketIOClient from "socket.io-client";
import { useParams } from 'react-router';
import { Link } from 'react-router-dom';
import { IsLoggedContext, loggedAsContext } from '../../../Shared/Contexts/IsLoggedContext';
const ENDPOINT = "http://127.0.0.1:5050";
// "https://cors-anywhere.herokuapp.com/"+

export const APIHeaders = {
    // 'Accept': 'application/json',
    'Accept': '*/*',
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    
  };



export function MessagesPage(){
    const { isLogged, setIsLogged } = useContext(IsLoggedContext);
    const { loggedAs, setLoggedAs } = useContext(loggedAsContext);
    const { messagesId } = useParams();
    const userId = getCookieUtil('userId');
    const [ messages, setMessages ] = useState([]);
    const history = useHistory();
    const [ allMessages, setAllMessages ] = useState([]);
    const [ dataLoaded, setdataLoaded ] = useState(false);
    const [response, setResponse] = useState("");
    const [isGuardian, setIsGuardian] = useState(true);
    const socket = socketIOClient(ENDPOINT,{
        withCredentials: true,
    });
    var mostrarMensajes = [];
    // const sendMessage = document.querySelector('.c-chat__textarea');

    let sortedMessages = []

    const loadMessages = () => {
        API.get('message/'+ messagesId).then(res => {
            setMessages(res.data.results[0])
            console.log(res.data.results[0]);
            setdataLoaded(true)
            if( messages.userMessages && messages.guardianMessages){
            SortMessages()
            }
        }).catch( err =>{
            console.log(err);
        });
    }
    
    function enterKeyPressed(){
        const sendMessage = document.querySelector('.c-chat__textarea');
        const sendValues = formatDate()+';;'+sendMessage.value;
        const valuesToSend = isGuardian ?{ guardianMessages: sendValues} : { userMessages: sendValues};
        SetSocket(valuesToSend)
        // sendMessagesToBack(valuesToSend);
        console.log(sendValues);
        sendMessage.value=""
    }

    function SortMessages(){
        // for(let i = 0; i < 100000000 ; i++){}
        if( messages.userMessages && messages.guardianMessages){
        if(userId == messages.guardian._id){setIsGuardian(true)}
        else{setIsGuardian(false)}
        }
        if( messages.userMessages){
            let userMessages = messages.userMessages
            userMessages.map(element => {
                const value = element.split(";;")
                const dataMess = {date:value[0],userMessage:value[1]}
                sortedMessages.push(dataMess)
                return 
            })
        }
        if( messages.guardianMessages){
            let guardianMessages = messages.guardianMessages
            guardianMessages.map(element => {
                const value = element.split(";;")
                const dataMess = {date:value[0],guardianMessage:value[1]}
                sortedMessages.push(dataMess)
                return 
            })
            sortedMessages = sortedMessages.sort((a, b) => new Date(b.date) - new Date(a.date) ) 
            // sortedMessages = sortedMessages.sort((a, b) => new Date('1970/01/01'+b.date) - new Date('1970/01/01'+a.date) ) 
            console.log('All messages Before Set',allMessages);
            setAllMessages(sortedMessages)
            mostrarMensajes = [...sortedMessages]
            console.log(mostrarMensajes);
            console.log('After set',allMessages);
            console.log('Sorted After Set',sortedMessages);
            // console.log(allMessages.length);
        }
    }
    




    // function sendMessagesToBack(dataToSend){
    //     // const mailId = '6065d4de18ea3b2f780950d9' ;
    //     API.patch('message/'+ mailId, dataToSend ).then(res => {
    //         console.log(res.data.results);
    //         setdataLoaded(false)
    //         setAllMessages([...sortedMessages, res.data.results])
            
    //     }).catch( err =>{
    //         console.log('Error');
    //         console.log(err);
    //     });
    // }


    function formatDate() {
        var d = new Date(),
            miliseconds = d.getSeconds(),
            minutes = d.getMinutes(),
            hour = d.getHours(),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
    
        if (month.length < 2) {month = '0' + month;}
        if (day.length < 2) {day = '0' + day;}
        return ([year, month, day].join('/')+' '+hour+':'+minutes+':'+miliseconds);
    }


    function SetSocket (dataToSend) {
        socket.emit("messageId",{ _id: messagesId , dataToSend, allMessages});
        GetSocket()
    }
    
    function GetSocket(){
        console.log('Entro a getSocket');
        socket.on("FromAPI", (response) => {
            // const dataReceived = data[0]
            // console.log('All Messages',allMessagess);
            console.log(response);
            if(messagesId == response.sendData._id){
                console.log('Es la sesion correcta');
                const addNewMessage = response.sendData.newMessage.userMessages ? response.sendData.newMessage.userMessages.split(";;") : response.sendData.newMessage.guardianMessages.split(";;");
                const dataMessage = response.sendData.newMessage.guardianMessages ? {date:addNewMessage[0],guardianMessage: addNewMessage[1]} : {date:addNewMessage[0], userMessage: addNewMessage[1]};
                // console.log(dataMessage);
            //     console.log('All Messagess',allMessagess);
                const addAllmessages = [...response.allMessages];
                
                addAllmessages.splice(0,0,dataMessage)
                // console.log('Add AllMsg',addAllmessages);
                setAllMessages(addAllmessages)
            }
            // console.log(data);
            // setResponse(data);
        });
    }

    
    useEffect( GetSocket , [] );
    // useEffect( SetSocket , [] );
    
    useEffect(loadMessages, [dataLoaded]);
    
    return(
        <div className="c-chat">
            {dataLoaded && 
            <>
            <div className="c-chat__headerMain">
                <div className="c-chat__header">
                    <Link to="/inbox"><img className="mainUser__img" src={logoArrow} alt="arrow"/></Link>
                    <h1 className="c-chat__title">{messages.title.split(';;')[0]}</h1>
                </div>
                <hr className="myInbox__hr"/>
            </div>
          
            <div className="c-chat__details">
                <div className="c-chat__reserva">
                    <p className="c-chat__status">Reserva {messages.status}</p>
                    <p className="c-chat__status orange">Detalles</p>
                </div>
                <p className="c-chat__makecontact" >Tu locker esta {messages.status}, ponte en contacto con 
                
                {userId == messages.guardian._id && <span > {messages.guardian.name} </span> }
                {userId == messages.user._id && <span > {messages.user.name} </span> }
                  para ultimar los detalles</p>
            </div>
            


        <div className="c-chat__scrollDiv">
            {allMessages.map((item)=>
            <div className="c-chat__body">
                
                {item.userMessage && userId != messages.user._id &&
                <div className="c-chat__image col-3">
                        <img className="c-chat__img" src={messages.user.image  ? messages.user.image : noImage } alt=""/>

                </div>}
                {item.guardianMessage && userId != messages.guardian._id &&
                <div className="c-chat__image col-3">
                        <img className="c-chat__img" src={messages.guardian.image  ? messages.guardian.image : noImage } alt=""/>

                </div>}
                

                <div className={item.userMessage ? "c-chat__message__user col-8" : "c-chat__message__guardian col-8" }>
                    <div className="c-chat__info">
                        <p className="c-chat__text">
                        {/* {userId == messages.guardian._id && <span > {messages.guardian.name} </span> }
                        {userId == messages.user._id && <span > {messages.user.name} </span> } */}
                        {item.guardianMessage && <span > {item.guardianMessage} </span> }
                        {item.userMessage && <span > {item.userMessage} </span> }
                        </p>
                    </div>

                </div>


                {item.guardianMessage && userId == messages.guardian._id &&
                <div className="c-chat__image col-3">
                        <img className="c-chat__img" src={messages.guardian.image  ? messages.guardian.image : noImage} alt=""/>

                </div>}
                {item.userMessage && userId == messages.user._id &&
                <div className="c-chat__image col-3">
                        <img className="c-chat__img" src={messages.user.image  ? messages.user.image : noImage} alt=""/>

                </div>}
            </div>)}
        </div>
            <div className="c-chat__textholder">
                <textarea placeholder="Escribe un mensaje..." className="c-chat__textarea" rows="5" cols="50" onKeyPress="3D" type="text" />
                <input className="c-chat__sendBtn" type="submit" onClick={enterKeyPressed}/>
            </div>
            </>
            }
            

        
        </div>
    )
}

