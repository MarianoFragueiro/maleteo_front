import { useContext, useEffect, useRef, useState } from "react";
import { useHistory, useParams } from "react-router";
import { API } from "../../../Shared/Services/Api";
import noImage from '../../../Assets/img/No_Image.jpg';
import Swiper from 'swiper';
import SwiperCore, { Navigation, Pagination } from 'swiper/core';
import arrowBack from '../../../Assets/Logos/arrow.png';

import { GuardianDetailsChatContext } from "../../../Shared/Contexts/GuardianDetailsChatContext";
import './PostDetails.scss';
import hosisSvg from '../../../Assets/Logos/hosiss.png';
import carla from '../../../Assets/img/carla.png';
import maria from '../../../Assets/img/maria.png';
import robert from '../../../Assets/img/robert.png';

import imgCasa1 from '../../../Assets/img/imgCasa1.jpeg';
import imgCasa2 from '../../../Assets/img/imgCasa2.jpeg';
import { Link } from "react-router-dom";
import { IsLoggedContext, loggedAsContext } from "../../../Shared/Contexts/IsLoggedContext";
import { getCookieUtil } from "../../../Shared/Utils/getCookieUtil";





export function PostDetails(){
    const { isLogged, setIsLogged } = useContext(IsLoggedContext);
    const { loggedAs, setLoggedAs } = useContext(loggedAsContext);
    const history = useHistory();
    const { setGuardianDetailsChatContext } = useContext(GuardianDetailsChatContext);

    let img = [];
    let imgLength = 0;

    // const myGuardianDetailsChatContextObject = {};

    const swipeRef = useRef(null);

    const {postId} = useParams();
    const [ postDetails, setPostDetails ] = useState([]);
    const [imgDetails, setImgDetails] = useState({})
    const [imgDetailsLenght, setImgDetailsLenght] = useState(0)

    let myPostGuardianDetails =[];

    const getGuardianPostDetails = () => {

        API.get('post/'+postId).then(res =>{
            myPostGuardianDetails = res.data.results
            console.log("CARGA DE RESULTADOS")
            console.log(myPostGuardianDetails)
            console.log(myPostGuardianDetails[0].images)
            setPostDetails(myPostGuardianDetails)
            setImgDetails(myPostGuardianDetails[0].images)
            setImgDetailsLenght(myPostGuardianDetails[0].images.length)

            console.log('Image Details',imgDetails)
            console.log('Image Details Lenght',imgDetailsLenght)
            // img = imgDetails;
            // imgLength = imgDetails.length;
            // console.log('img',img);
            // console.log('imageLength',imgLength);
            })
            
    }

    

    // console.log(postDetails[0].images[0])

    const createSlider =  ()=>{
        const swiperContainer$$ = swipeRef.current;
        // const swiperContainer$$ = document.querySelector('.swiper-container');
        
    // const swiper = 
    new Swiper(swiperContainer$$, {
        // Optional parameters
        direction: 'horizontal',
        slidesPerView: 'auto',
        spaceBetween: 30,
      
        // If we need pagination
        pagination: {
          el: '.swiper-pagination',
          type: 'bullets',
          clickable: true,
        },
      
      });
    }


    function verifyToken(){
        setLoggedAs(getCookieUtil('loggedAs'))
        setIsLogged(!!getCookieUtil('isLogged'))
        console.log('Is Logged',isLogged);
        console.log('LoggedAs',loggedAs);
    //     API.post('init').then(res => {
    //         document.cookie ='isLogged= true';
    //         document.cookie ='loggedAs= user';
    //         setIsLogged(true)
    //     }).catch( err =>{
    //         document.cookie = 'isLogged= false';
    //         setIsLogged(false)
    //         history.push('login')
    //         });
        }
    
        
        
    useEffect(verifyToken,[]);
        
    useEffect(createSlider, [])
    useEffect(() => getGuardianPostDetails(), []);
    
    return(
        <div className="postDetails">
            <Link to="">
                <img className="postDetails__arrowBack" src={arrowBack} alt="" />
            </Link>

            <div className="swiperDetails">
                <div ref={swipeRef} class="swiper-container">
                    <div class="swiper-wrapper">
                    {imgDetailsLenght >0 &&
                    imgDetails.slice(0 , imgDetailsLenght).map((img, i ) => <div class="swiper-slide"><img className="imagenes" src={img ? img: noImage } alt="" />
  
                        </div>)
                    }
                    {imgDetailsLenght ==0 &&
                        <div class="swiper-slide"><img className="imagenes" src={ noImage } alt="" />
                        </div>
                    }

                    </div>
                    <div class="swiper-pagination"></div>
                    {/* <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>     */}
                </div>
            </div>
            
            {postDetails.map((item,i)=>
            
            <div className="postDetails__main" key={i}>
                <div className="postDetails__user">
                    <div className="postDetails__info">
                   
                        <h2 className="postDetails__h2">{item.title}</h2>
                        <p className="postDetails__p">{item.location}</p>
                        <p className="postDetails__p">Guardian: {item.user.name}</p>
                    </div>
                    
                    <div className="postDetails__object">
                        <img className="postDetails__img" src={item.user.image ?item.user.image : noImage} alt="" />{item.image}
                    </div>
                </div>
                <div className="section">
                    <div className="section__div">
                        <img className="section__img" src={hosisSvg} alt=""/>
                        <div className="section__text">
                            <h2 className="section__h2">Tipo de locker</h2>
                            <p className="section__p">El salón de su casa será el lugar idóneo para alojar tus maletas y cuidar de ellas.</p>
                        </div>
                    </div>
                    <div className="section__div">
                        <img className="section__img" src={hosisSvg} alt=""/>
                        <div className="section__text">
                            <h2 className="section__h2">Como la patena</h2>
                            <p className="section__p">19 usuarios recientes han catalogado su espacio como muy limpo.</p>
                        </div>
                    </div>
                    <div className="section__div">
                        <img className="section__img" src={hosisSvg} alt=""/>
                        <div className="section__text">
                            <h2 className="section__h2">Un Fortín</h2>
                            <p className="section__p">El 95% de los usuarios han valorado su experiéncia como muy segura.</p>
                        </div>
                    </div>
                </div>
                <hr className="section__hr"/>
                <div>
                    <p className="section__info">Habitación espaciada a 15 minutos del centro de madrid y a 5 minutos del la Linea 1. Además ofrezco información turística personalizada.</p>
                </div>
                <hr className="section__hr"/>
                <div>
                    {/* aqui va el mapa */}
                </div>

                <div className="reviews">
                <h2 className="section__h2">Reseñas</h2>
                    <div className="reviews__sec">
                        {/* <img>imagen por defecto</img> */}
                        <img className="reviews__img" src={maria} alt="" />
                        <div className="reviews__div">
                            <div className="reviews__titles">
                                <h3 className="reviews__h3">Maria</h3>
                                <h5 className="reviews__h5">En julio de 2019</h5>
                            </div>
                            <div className="reviews__text">
                                <p className="reviews__h5">El hall es acogedor y super chulo, muy limpio, Marta nos ayudó a subir las maletas y nos transmitió muchísima seguridad.</p>
                            </div>
                        </div>
                    </div>
                    <hr className="section__hr"/>
                    <div className="reviews__sec">
                        {/* <img>imagen por defecto</img> */}
                        <img className="reviews__img" src={robert} alt="" />
                        <div className="reviews__div">
                            <div className="reviews__titles">
                                <h3 className="reviews__h3">Robert</h3>
                                <h5 className="reviews__h5">En julio de 2019</h5>
                            </div>
                            <div className="reviews__text">
                                <p className="reviews__h5">Marta is very nice and her space is so cozy, she also showed us the best places to go for tapas in Madrid. Thank you so much.</p>
                            </div>
                        </div>
                    </div>
                    <hr className="section__hr"/>
                    <div className="reviews__sec">
                        {/* <img>imagen por defecto</img> */}
                        <img className="reviews__img" src={carla} alt="" />
                        <div className="reviews__div">
                            <div className="reviews__titles">
                                <h3 className="reviews__h3">Carla</h3>
                                <h5 className="reviews__h5">En junio de 2019</h5>
                            </div>
                            <div className="reviews__text">
                                <p className="reviews__h5">Marta responde rápido y estuvo muy atenta. Nos dió muchos consejos sobre Madrid y pudimos hacer turismo tranquilamente. Su ubicación nos vino genial para el transporte de vuelta.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <hr className="section__hr"/>

                <div className="rules">
                    <h2 className="section__h2">Cómo debe ser su maleta</h2>
                    <hr className="section__hr"/>
                    <h2 className="section__h2">Tipo de cancelación de reserva</h2>
                    <hr className="section__hr"/>
                    <h2 className="section__h2">Contactar con tu guardián</h2>
                    <hr className="section__hr"/>
                    <h2 className="section__h2">Denunciar anuncio</h2>
                    <hr className="section__hr"/>
                    {/* <h2>Otros lockers cerca de ti</h2> */}
                    {/* no creo que de tiempo a hacer la cercania y mostrar sus fotos */}
                </div>

                <div className="total">
                    <div>
                        <h2 className="section__h2">Total 12$</h2>
                        <a className="total__link" href="">despliegue del precio</a>
                    </div>
                    <Link to="/bookingdetails"><button onClick={()=> setGuardianDetailsChatContext(item)} className="total__btn">Reservar ahora</button></Link>
                </div>
            </div>)}
        
        </div>
    )
}