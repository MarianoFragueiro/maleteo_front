import React from 'react';

export const loggedAsContext = React.createContext();

export const IsLoggedContext = React.createContext();

export const tokenValue = React.createContext();