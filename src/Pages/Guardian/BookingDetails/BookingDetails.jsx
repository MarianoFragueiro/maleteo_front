import { useContext, useEffect } from "react";
import { Link, useHistory } from "react-router-dom";
import { API } from "../../../Shared/Services/Api"
import { getCookieUtil } from "../../../Shared/Utils/getCookieUtil";
import { GuardianDetailsChatContext } from "../../../Shared/Contexts/GuardianDetailsChatContext";
import './BookingDetails.scss';
import arrowBack from '../../../Assets/Logos/arrow.png';
import { IsLoggedContext, loggedAsContext } from "../../../Shared/Contexts/IsLoggedContext";



export function BookingDetails(){
    const { isLogged, setIsLogged } = useContext(IsLoggedContext);
    const { loggedAs, setLoggedAs } = useContext(loggedAsContext);
    const history = useHistory();

    const numbreBaggage = getCookieUtil("baggage")
    const dateArrive = getCookieUtil("arrive")
    const dateCollect = getCookieUtil("collect")
    console.log(getCookieUtil())
    const { guardianDetailsChatContext } = useContext(GuardianDetailsChatContext)    
    console.log("Los datos del guardian de la reserva, los del usuario que esta haciendo la reserva estan en las cookies")
    console.log(guardianDetailsChatContext)



    
    const dataOfGuardianAndPostToSend = {
        title:guardianDetailsChatContext.title+";;"+guardianDetailsChatContext.user._id +","+getCookieUtil("userId"),
        user:getCookieUtil("userId"),
        guardian:guardianDetailsChatContext.user._id,
        status:"pendiente",
        date: [dateArrive,dateCollect]

    }
    const confirmReservation = () => {
        // console.log(formData);
        API.post('message', dataOfGuardianAndPostToSend).then(res => {
            
            console.log(dataOfGuardianAndPostToSend);
            console.log("TU RESERVA HA SIDO REALIZADA")
            
        }).catch( err =>{
            console.log("erroraco")
            // setErrorLogin( err.response.data.results);
            // setUserExists( false);
            console.log(err);
        });
    }



    //FUNCION FECHA ACTUAL PARA LA CREACION DE LA RESERVA
    function formatDate() {
        var d = new Date(),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
    
        if (month.length < 2) 
            month = '0' + month;
        if (day.length < 2) 
            day = '0' + day;
        // time =hour+':'+minutes+':'+miliseconds
        return ([year, month, day].join('/'));
    }

    function verifyToken(){
        API.post('init').then(res => {
            document.cookie ='isLogged= true';
            document.cookie ='loggedAs= user';
            setIsLogged(true)
            setLoggedAs('user')
            console.log(isLogged);
            console.log(loggedAs);
            }).catch( err =>{
            document.cookie = 'isLogged= false';
            setIsLogged(false)
            history.push('login')
            });
        }
    
    useEffect(verifyToken,[]);


return(
    <div className="myBookingDetails">
    <div className="bookingDetails">
        <img className="bookingDetails__arrowBack" src={arrowBack} alt="back"/>
        <div>
            <h3 className="bookingDetails__h3">Detalles de tu reserva</h3>
        </div>
        <div className="bookingDetails__main">
            <div className="bookingDetails__titles">
                <p className="bookingDetails__subtitles">LLegada</p>
                <p className="bookingDetails__text">{dateArrive}</p>
            </div>
            <div className="bookingDetails__titles">
                <p className="bookingDetails__subtitles">Recogida</p>
                <p className="bookingDetails__text">{dateCollect}</p>
            </div>
            <div className="bookingDetails__titles">
                <p className="bookingDetails__subtitles">Equipaje</p>
                <p className="bookingDetails__text">{numbreBaggage}</p>
            </div>
        </div>
        <hr className="bookingDetails__hrs"/>
        <div className="bookingDetails__check">
            <div className="bookingDetails__details">
                <p className="bookingDetails__p">Precio por 2 equipajes 6,00 x 2</p>
                <p className="bookingDetails__num">10€</p>
            </div>
            <div className="bookingDetails__details">
                <p className="bookingDetails__p">Gastos de gestión</p>
                <p className="bookingDetails__num">2€</p>
            </div>
            <div className="bookingDetails__details">
                <p className="bookingDetails__p">Servicio asegurado hasta 1000€</p>
                <p className="bookingDetails__num">Gratis</p>
            </div>
            <div className="bookingDetails__details">
                <p className="bookingDetails__p">Total</p>
                <p className="bookingDetails__num">12€</p>
            </div>
        </div>
        <div className="bookingDetails__main">
        <Link to="/userhome">
            <button className="bookingDetails__bookBtn" onClick={() => confirmReservation()}>Reservar</button>
        </Link>
        </div>
        



    </div>
    </div>
)
}