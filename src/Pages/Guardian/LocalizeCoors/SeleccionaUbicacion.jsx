import { Link } from "react-router-dom";
import { MapLocalizaCoors } from "./MapLocalizaCoors";
import arrow from '../../../Assets/Logos/leftsolid.svg';
import continueBtn from '../../../Assets/Logos/boton-continuar.png';
import { API } from "../../../Shared/Services/Api";
import { useHistory } from "react-router";
import './SeleccionaUbicacion.scss';
import { useContext, useEffect } from "react";
import { IsLoggedContext, loggedAsContext } from "../../../Shared/Contexts/IsLoggedContext";

export function SeleccionaUbicacion(){

    const history = useHistory();
    const { isLogged, setIsLogged } = useContext(IsLoggedContext);
    const { loggedAs, setLoggedAs } = useContext(loggedAsContext);

    function verifyToken(){
        API.post('init').then(res => {
            document.cookie ='isLogged= true';
            // document.cookie ='loggedAs= user';
            setIsLogged(true)
            }).catch( err =>{
            document.cookie = 'isLogged= false';
            setIsLogged(false)
            history.push('login')
            });
        }
    
    useEffect(verifyToken,[]);

    return(
    <div>
        <div>
            <div className="topbar">
                <Link to="./userpage">
                    <img className="topbar__arrow" src={arrow} alt="back"/>
                </Link>
               
                
            </div>
            <div>
            
            <MapLocalizaCoors/>
            </div>

        </div>
        <div>
            <Link to="/becomeguardian">
                <img className="topbar__continue" src={continueBtn} alt=""/>
            </Link>
        </div>
    </div>

    )
}