

import logoArrow from '../../../Assets/Logos/arrow.png';
import noImage from '../../../Assets/img/No_Image.jpg';
import {  useEffect, useState } from 'react';
import { API } from '../../../Shared/Services/Api';
import { getCookieUtil } from '../../../Shared/Utils/getCookieUtil';
import socketIOClient from "socket.io-client";
import { useParams } from 'react-router';
import { Link } from 'react-router-dom';
const ENDPOINT = "http://127.0.0.1:5050";
// "https://cors-anywhere.herokuapp.com/"+

export const APIHeaders = {
    // 'Accept': 'application/json',
    'Accept': '*/*',
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    
  };



export function MessagesPage2(){
    const { messagesId } = useParams();
    const [ dataLoaded, setdataLoaded ] = useState(false);
    const [response, setResponse] = useState([]);
    const socket = socketIOClient(ENDPOINT,{
        withCredentials: true,
    });




    function SetSocket () {
        socket.emit("messageId",{ _id: messagesId });
        GetSocket()
    }
    
    function GetSocket(){
        socket.on("FromAPI", data => {
                const dataReceived = data[0]
                if(messagesId == dataReceived._id){
                    console.log('Es la sesion correcta');
                }
                console.log(data);
                // setResponse(data);
            });
    }

    // useEffect( GetSocket , [] );
    useEffect( SetSocket , [] );
    return(
        <div className="c-chat">
        se carga
        {response}
            {dataLoaded &&
            <>
                <div className="c-chat__header">
                <Link to="/userpage"><img className="mainUser__img" src={logoArrow} alt="arrow"/></Link>

            </div>
                <hr className="myInbox__hr"/>
            <div className="c-chat__details">
                <div className="c-chat__reserva">

                    <p className="c-chat__status orange">Detalles</p>
                </div>

            </div>


        </>}
        </div>
    )
}