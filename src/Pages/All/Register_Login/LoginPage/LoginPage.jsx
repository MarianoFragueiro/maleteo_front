import React, { useContext, useState } from 'react'
import { useForm } from "react-hook-form";
import { IsLoggedContext, loggedAsContext } from '../../../../Shared/Contexts/IsLoggedContext';
import { API } from "../../../../Shared/Services/Api";
//styles
import './LoginPage.scss';
import facebook from '../../../../Assets/Logos/facebook.png';
import goog from '../../../../Assets/Logos/google.png';
import { Link } from 'react-router-dom';

import io from 'socket.io-client'
import { useHistory } from 'react-router';


const ENDPOINT = "http://127.0.0.1:4001";
// const socket = io('http://127.0.0.1:5050');
// const providers = [ 'register/google', 'register/facebook'];

export function LoginPage() {
    const { isLogged, setIsLogged } = useContext(IsLoggedContext);
    const { loggedAs, setLoggedAs } = useContext(loggedAsContext);
    const [response, setResponse] = useState("");
    const history = useHistory();

    // checkPopup() {
    //     const check = setInterval(() => {
    //       const { popup } = this
    //       if (!popup || popup.closed || popup.closed === undefined) {
    //         clearInterval(check)
    //         this.setState({ disabled: ''})
    //       }
    //     }, 1000)
    //   }

    // let userExists = true;
    // let errorLogin = "" ;

    const [errorLogin, setErrorLogin]= useState("");
    const [userExists, setUserExists]= useState(true);
    const [underline, setUnderline]= useState("x");

    const { register, handleSubmit } = useForm();
    // const {setIsLogged} = useContext(loggedAsContext);

    const [next, setNext]= useState("Login");

    function invalid(e) {
        if (!/PATTERN/.test(e.target.value)) { // somehow validity.valid returns a wrong value
          e.target.setCustomValidity('Su contraseña no es correcta')
        } else {
          e.target.setCustomValidity('')
        }
      }


    const loginClick = formData => {
        // console.log(formData);
        API.post('login', formData).then(res => {
            console.log(res.data.results);
            const token = res.data.results.token;
            const userId =  res.data.results.user;
            document.cookie = 'userId=' + userId;
            setIsLogged(true)
            history.push('/gettoken/'+token);
        }).catch( err =>{
            // setErrorLogin( err.response.data.results);
            // setUserExists( false);
            console.log(err);
        });
    }

    const registerClick = formData => {
        console.log(formData);
        API.post('register', formData).then(res => {
            console.log(res.data.results);
            const token = res.data.results.token;
            const userId =  res.data.results.user;
            setIsLogged(true)
            history.push('/gettoken/'+token)
        }).catch( err =>{
            console.log('Error');
            console.log(err);
        });
    }


    const underlineInit = ()=>{setUnderline("x"); }
    const underlineRegister = ()=>{setUnderline("y");}
    setIsLogged(false)
    setLoggedAs('user')


    return (
        
<div className="myContainer">
    <div className="mainDiv">
        <div className="header">
            {/* <img className="header__arrow" src={arrowBack} alt="arrowBack"/> */}
            <section className="header__sec">
                <button onClick={()=> {setNext("Login"); underlineInit();}} className={underline === 'x' ? "btnUnder" : "header__btn" }>Iniciar sesión</button>
                <button onClick={()=> {setNext("Register"); underlineRegister();}} className={underline === 'y' ? "btnUnder" : "header__btn"  }>Regístrate</button>
            </section>
            
        </div>
        <div className="social">
        {next === "Login" && 
                <h1 className="social__h1">Inicia sesión ahora</h1>
        }
        {next === "Register" && 
                <h1 className="social__register">Únete a Maleteo y disfruta de sus ventajas </h1>
        }
        {next === "Login" &&
            <div className="social__div">
                <a href={process.env.REACT_APP_BACK_URL+'login/facebook'}  className="social__facebook"><img className="facebookImg" src={facebook} alt="facebook" />Facebook</a>
                <a href={process.env.REACT_APP_BACK_URL+'login/google'} className="social__google"><img className="googleImg" src={goog} alt="goog" />Google</a>
            </div>
        }
        {next === "Register" &&
            <div className="social__div">
                <a href={process.env.REACT_APP_BACK_URL+'register/facebook'} className="social__facebook"><img className="facebookImg" src={facebook} alt="facebook" />Facebook</a>
                <a href={process.env.REACT_APP_BACK_URL+'register/google'} className="social__google"><img className="googleImg" src={goog} alt="goog" />Google</a>
            </div>
        }
            <p className="social__text">o utiliza tu correo electrónico</p>
        </div>

        {next === "Login" && 
            <form className="loginForm"  onSubmit={handleSubmit(loginClick)}>
                {/* register your input into the hook by invoking the "register" function */}
                <label className="loginForm__label" htmlFor="email">Dirección de correo electrónico</label>
                <input className="loginForm__input" name="email" id="email" placeholder="andresmoreno@contact.com"
                    ref={register({ required: true, pattern: /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/ })}/>
                    {!userExists &&
                        <p className="user__error">{errorLogin}</p>}
                {/* include validation with required or other standard HTML validation rules */}
                <label className="loginForm__label" htmlFor="password">Contraseña</label>
                <input className="loginForm__input" name="password" id="password" type="password" defaultValue="ABCedf123"
                    ref={register({
                        required: true,
                        pattern: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/ 
                    })} /> {/* errors will return when field validation fails  */}
                {/*{errors.exampleRequired && <span>This field is required</span>}*/}
                <div className="sub">
                 <input className="sub__btn" type="submit" value="Inicia sesión"/>
                </div>
            </form>
            }


        {next === "Register" && 
            
            <form className="loginForm" onSubmit={handleSubmit(registerClick)}>

                <label className="loginForm__label" htmlFor="email">Dirección de correo electrónico</label>
                <input className="loginForm__input" name="email" id="email" placeholder="andresmoreno@contact.com"
                    ref={register({ required: true, pattern: /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/ })}/>

                {/*<label htmlFor="username">Username</label>*/}
                {/*<input name="username" id="username" defaultValue="abelcabezaroman"*/}
                {/*       ref={register({ required: true, minLength: 4 })}/>*/}

                {/* register your input into the hook by invoking the "register" function */}
                    <label className="loginForm__label" htmlFor="name">Nombre</label>
                <input className="loginForm__input" name="name" id="name" placeholder="Andrés"
                    ref={register({ required: true })}/>

                {/*<input name="role" id="role" defaultValue="admin"*/}
                {/*       ref={register({ required: true })}/>*/}

                {/* register your input into the hook by invoking the "register" function */}
                <label className="loginForm__label" htmlFor="surname">Apellido</label>
                <input className="loginForm__input" name="surname" id="surname" placeholder="Moreno"
                    ref={register({ required: true })}/>

                {/*<input name="role" id="role" defaultValue="admin"*/}
                {/*       ref={register({ required: true })}/>*/}            

                {/* include validation with required or other standard HTML validation rules */}
                <label className="loginForm__label" htmlFor="password">Contraseña</label>
                <input className="loginForm__input" name="password" id="password" type="password" defaultValue="ABCedf123"
                    ref={register({ required: true, pattern: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/ })} onInvalid={invalid} onInput={invalid} />
                {/* errors will return when field validation fails  */}
                {/*{errors.exampleRequired && <span>This field is required</span>}*/}

                <label  className="date"  htmlFor="BirthDate">Fecha de nacimiento</label>
                <p className="date__p">Para registrarte tendrás que ser mayor de edad. Los usuarios no veran tu fecha de cumpleaños</p>
                <input  className="loginForm__input" type="date" name="BirthDate" id="BirthDate"  
                    ref={register({ required: true })}/>
                <div className="d-flex justify-content-center align-items-center
                ">
                    <input className="loginForm__input" type="checkbox" name="checkboxAccept" ref={register({ required: true })}/><p className="loginForm__text">Quiero recibir consejos sobre como gestionar mi equipaje, ofertas, novedades y totros correos electrónicos de Maleteo</p>
                </div>  

                <div className="sub">
                <input className="sub__btn" type="submit" value="Register"/>
                </div>
            </form>    
            }  
    </div>
    
    </div>  
    )
}
