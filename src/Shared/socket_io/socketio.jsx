import React, { useEffect, useState } from "react";
import socketIOClient from "socket.io-client";
const ENDPOINT = "http://127.0.0.1:5050";
// "https://cors-anywhere.herokuapp.com/"+

export const APIHeaders = {
    // 'Accept': 'application/json',
    'Accept': '*/*',
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    
  };


export function Socketio() {
  const [response, setResponse] = useState("");

//   const socket = socketIOClient(ENDPOINT,{
//     extraHeaders: {
//         APIHeaders
//       }
// });

    function GetSocket(){
        const socket = socketIOClient(ENDPOINT, {
            withCredentials: true,
        });
        socket.on("FromAPI", data => {
              setResponse(data);
            });
    }

  useEffect( GetSocket , [] );



  return (
    <p>
      It's <time dateTime={response}>{response}</time>
    </p>
  );
}


