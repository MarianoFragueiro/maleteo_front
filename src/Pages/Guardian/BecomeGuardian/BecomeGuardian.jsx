import './BecomeGuardian.scss';
import vector from "../../../Assets/Logos/Vector.svg";
import logox from "../../../Assets/Logos/XX.png";

import { useContext, useEffect, useState } from 'react';
import { InputNumber } from 'primereact/inputnumber';
import axios from 'axios';
import { API } from '../../../Shared/Services/Api';
import { useHistory } from 'react-router';
import { getCookieUtil } from '../../../Shared/Utils/getCookieUtil';
import { CoordenadasContext } from '../../../Shared/Contexts/Coordenadas';
import { Link } from 'react-router-dom';
import { IsLoggedContext, loggedAsContext } from '../../../Shared/Contexts/IsLoggedContext';



export function BecomeGuardian(){

    var location="";
    var title="";
    var hoursDeposit="";
    var hoursCollect="";
    var services="";
    var home="";
    var space="";
    // var images=[];


    const { coors } = useContext(CoordenadasContext);

    const history = useHistory();

    const [timeDeposit, setTimeDeposit] = useState();
    const [timeCollect, settimeCollect] = useState();
    const [images, setImages] = useState([]);

    const [boton, setBoton] = useState(null);
    const [boton2, setBoton2] = useState(null);

    const[archivos, setArchivos]=useState(null);

    const { isLogged, setIsLogged } = useContext(IsLoggedContext);
    const { loggedAs, setLoggedAs } = useContext(loggedAsContext);
    


    const showHome =()=>{

        const backGrey = document.getElementById('showBtn');
        backGrey.classList.remove("hidden")
    }

    const showSpace =()=>{

        const backGrey = document.getElementById('showBtnSpace');
        backGrey.classList.remove("hidden")
    }

    const hidde =()=>{
        const backGrey = document.getElementById('showBtn');
        backGrey.classList.add("hidden");
    }
    const hidde2 =()=>{
        const backGrey = document.getElementById('showBtnSpace');
        backGrey.classList.add("hidden");
    }


    const sendValues = ()=>{

        const userId= getCookieUtil("userId");

        location = document.getElementById("location").value;
        title = document.getElementById("title").value;        
        hoursDeposit = document.getElementById("hoursDeposit").value;        
        hoursCollect = document.getElementById("hoursCollect").value;        
        services = document.getElementById("services").value;        
        home = document.getElementById("home").value;        
        space = document.getElementById("space").value;        

        console.log(location);
        console.log(title);
        console.log(hoursDeposit);
        console.log(hoursCollect);
        console.log(services);
        console.log(home);
        console.log(space);
        console.log(coors.lng)

        const sendValuesEnd={

            "user": userId,
            "location": coors.direcionGuardian,
            "title": title,
            "hoursDeposit": hoursDeposit,
            "hoursCollect": hoursCollect,
            "services": services,
            "home": home,
            "space": space,
            "latPosition":coors.lat,
            "lngPosition":coors.lng,
            "images":images
        };
        console.log(sendValuesEnd)

       

        API.post('post', sendValuesEnd).then(res => {
            console.log("hola")
            console.log(res.data.results);
            document.cookie="postId="+res.data.results.id;
        })
        .catch( err =>{
            // setErrorLogin( err.response.data.results);
            // setUserExists( false);
            console.log(err);
        });

    }

    const home1 =()=>{ setBoton("Casa"); hidde()}
    const home2 =()=>{setBoton("Apartamento"); hidde()}
    const home3 =()=>{setBoton("Establecimiento"); hidde();}

    const space1 =()=>{setBoton2("Habitación"); hidde2();}
    const space2 =()=>{setBoton2("Hall"); hidde2();}
    const space3 =()=>{setBoton2("Trastero"); hidde2();}
    const space4 =()=>{setBoton2("Buhardilla"); hidde2();}
    const space5 =()=>{setBoton2("Garaje"); hidde2();}

    const subirArchivos=e=>{
        setArchivos(e);
    }

    const insertarArchivos=async()=>{
        const f = new FormData();

        for (let index=0; index < archivos.length; index++){
            f.append("image", archivos[index]);
        }

        API.post("post/image/",f)
        .then(response=>{
            setImages([...images,response.data.results]);
            // console.log(response.data);
            console.log(images);
        }).catch(error=>{
            console.log(error);
        })
    }
    // useEffect(()=>{console.log(coors)})
    // console.log("hola")
    // console.log(coors.direcionGuardian)
    // console.log("hola")
    // console.log(images)

    function verifyToken(){
        API.post('init').then(res => {
            document.cookie ='isLogged= true';
            // document.cookie ='loggedAs= user';
            setIsLogged(true)
            }).catch( err =>{
            document.cookie = 'isLogged= false';
            setIsLogged(false)
            history.push('login')
            });
        }
    
    useEffect(verifyToken,[]);
    
    
    return(
    <div className="becomeGuardianCenter">
    <div className="becomeContainer">

        <div className="becomeContainer__title">
            <Link to="/userpage"><div className="becomeContainer__logoDiv">
            <img src={logox} alt="close" className="becomeContainer__logo"/>
            </div></Link>
            <h2 className="becomeContainer__h2"> Configura tu espacio en muy pocos pasos</h2>
        </div>
        
        <div className="becomeContainer__form">

                <input className="becomeContainer__input"  type="text" id="location" readOnly="readonly" placeholder={coors.direcionGuardian} />
                <input className="becomeContainer__input" type="text" id="title" placeholder="Título" />

                <InputNumber inputId="hoursDeposit" min={0} max={24}  defaultValue={timeDeposit} onValueChange={(e) => setTimeDeposit(e.value)} showButtons />
                <InputNumber inputId="hoursCollect" min={0} max={24}  defaultValue={timeCollect} onValueChange={(e) => settimeCollect(e.value)} showButtons />
                
                <input className="becomeContainer__input" type="text" id="services" placeholder="Servicios" />
                

                <input className="becomeContainer__input" onClick={showHome} type="sumbit" id="home" value = {boton==null ? "Especifica tu propiedad" : boton}/>

                <div id="showBtn" className="hidden back__grey">

                    <div className="hiddenProperty" id="propiedad__show">
                        <span onClick={home1} className="btn">Casa</span>
                        <span onClick={home2} className="btn">Apartamento</span>
                        <span onClick={home3} className="btn">Establecimiento</span>
                    </div>
                </div> 

                
                <input className="becomeContainer__input" onClick={showSpace} type="sumbit" id="space" value = {boton2==null ? "¿Qué tipo de espacio?" : boton2}/>

                <div id="showBtnSpace" className="hidden back__grey">

                    <div className="hiddenProperty" id="propiedad__show">
                        <span onClick={space1} className="btn">Habitación</span>
                        <span onClick={space2} className="btn">Hall</span>
                        <span onClick={space3} className="btn">Trastero</span>
                        <span onClick={space4} className="btn">Buhardilla</span>
                        <span onClick={space5} className="btn">Garaje</span>
                    </div>
                </div>
                
                    {images.map((i) =>
                    <img className="becomeContainer__img" src={i} />
                    )
                    }
                <div className="becomeButtons">
                    <label className="labelPhotos" htmlFor="photos"><img className="svg" src={vector} alt="hola" />Fotos</label>

                    <input className="photos" name="photos" id="photos"  type="file" multiple onChange={(e)=>subirArchivos(e.target.files)} />
                    <button className="becomeButtons__upload" onClick={()=>insertarArchivos()}>Añadir foto</button>
                </div>
                <div className="continueButton">
                    <Link to="/postguardian">
                    <button className="continueButton__btn" onClick={sendValues}>Continuar</button>
                    </Link>
                </div>
        </div>
    </div>       
        
    </div>

    )
}
