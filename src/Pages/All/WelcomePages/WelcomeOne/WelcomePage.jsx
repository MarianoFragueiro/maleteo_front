import { Link, NavLink, useHistory } from 'react-router-dom';
import React, {useContext, useEffect, useState} from 'react';
import logo from '../../../../Assets/Logos/cadena.png';
import image from '../../../../Assets/Logos/world.png'; 
import './WelcomePage.scss';
import { getCookieUtil } from '../../../../Shared/Utils/getCookieUtil';
import { IsLoggedContext } from '../../../../Shared/Contexts/IsLoggedContext';


export function WelcomePage(){

    const { isLogged, setIsLogged } = useContext(IsLoggedContext);
    const history = useHistory();
    const [next, setNext]= useState("FirstCard");
   
    const getCookie =()=>{
        const token = getCookieUtil('token');
        const userId = getCookieUtil('userId');
        console.log(userId);
        console.log(token);
        if( getCookieUtil('isLogged') == 'true'){
            setIsLogged(true)
            history.push('/userhome');
        }
      }


    useEffect(getCookie,[]);


    return(
     <div className="main">
     
        {next === "FirstCard" && 
        <div className="welcome">
        <div className="welcome__div">
            <img className="welcome__img" src={logo} alt="logo"/>
            <p className="welcome__title">
                Prepárate para liberarte de tu equipaje
            </p>
            <p className="welcome__subtitle">
                Encuentra a tu guardián y disfruta a tu manera. Miles de usuarios ya están aprovechando las ventajas.
            </p>
            <button onClick={()=> setNext("SecondCard")} className="welcome__btn">Continuar</button>
            {/* <button className="welcome__btn">Continuar</button> */}
            {/* <NavLink className="c-menu__link"   exact to="/welcome"><button className="welcome__btn">Continuar</button></NavLink> */}
        </div>
        </div>
        }
        
        {next === "SecondCard" && 
        <div className="welcome">
        <div className="welcome__div">
            <img className="welcome__img" src={image} alt="logo"/>
            <p className="welcome__title">
                El mismo precio
                en cualquier parte
            </p>
            <p className="welcome__subtitle">
                Dispondrás de un precio fijo estés donde estés sin importar el tamaño o el peso.
            </p>
            <Link to="/login"><button className="welcome__btn">Empezar Ya</button></Link>
            {/* <Link className="c-menu__link"   exact to="/login"><button className="welcome__btn">Empezar Ya</button></Link> */}
            {/* <NavLink className="welcome__link" exact to="/login">Consulta los precios<hr/></NavLink> */}
        </div>
        </div>
        }

        
     </div>
     

    )
}