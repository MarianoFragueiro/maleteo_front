import { useContext, useEffect, useState } from "react";
import { useHistory, useParams } from "react-router";
import { IsLoggedContext, loggedAsContext, tokenValue } from "../../../Shared/Contexts/IsLoggedContext";
import socketIOClient from "socket.io-client";


  const ENDPOINT = "http://127.0.0.1:5050";
  // "https://cors-anywhere.herokuapp.com/"+
  
  export const APIHeaders = {
      // 'Accept': 'application/json',
      'Accept': '*/*',
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      
    };

export function GetTokenGoogleFace(){
    const { userId,token } = useParams();
    const { isLogged, setIsLogged } = useContext(IsLoggedContext);
    const { loggedAs, setLoggedAs } = useContext(loggedAsContext);
    const { tokenValuee, setTokenValuee } = useContext(tokenValue);
    // const { tokenSet, setTokenSet } = useContext(tokenValue);
    const [ receiveToken, setReceiveToken ] = useState(false);
    const history = useHistory();

    // document.cookie = 'token=' + token;
    const socket = socketIOClient(ENDPOINT,{
        withCredentials: true,
    });


    setIsLogged(true)
    setLoggedAs('user')
    
    function SetSocket (dataToSend) {
        socket.emit("GoogleOrFacebookTokenRequest",{_id: userId, token: token});
        GetSocket()
    }
    
    function GetSocket(){
        console.log('Entro a getSocket');
        socket.on("GoogleOrFacebookTokenRequest", response => {

            // setReceiveToken()
            console.log('Google Face');

            console.log('response',response);
            console.log('responseToken',response);
            document.cookie = 'token=' + response.token;
            setTokenValuee(response)
            console.log(tokenValuee);
            setReceiveToken(true)
            

        });
    }

    function redirect(){
        if(receiveToken){
            history.push('/userhome');
        }
    }



    useEffect( SetSocket , [] );
    useEffect( GetSocket , [] );
    useEffect( redirect , [receiveToken] );
    
    return(<>
        <div>
            black
            
        </div>
    </>)
}

