import { useContext, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { Navbar } from "../../../Core/Navbar/Navbar";
import { IsLoggedContext, loggedAsContext } from "../../../Shared/Contexts/IsLoggedContext";
import { API } from "../../../Shared/Services/Api";
import { useHistory } from "react-router";
import { getCookieUtil } from "../../../Shared/Utils/getCookieUtil";
import './UserPage.scss';
import vector from "../../../Assets/Logos/Vector.svg";

var image="";

export function UserPage(){

    const history = useHistory();

    const { isLogged, setIsLogged } = useContext(IsLoggedContext);
    const { loggedAs, setLoggedAs } = useContext(loggedAsContext);

    const[archivos, setArchivos]=useState(null);

    

    function changeToUserOrGuardian(){
        const value = getCookieUtil('loggedAs') == 'guardian' ?  'user' : 'guardian' ;
        document.cookie ='loggedAs=' + value;
        setLoggedAs(value)
        console.log(value);
    }

    function initUserPage(){
        const value =  getCookieUtil('loggedAs')
        document.cookie ='loggedAs=' + value;
        setLoggedAs(value)
    }

    const subirArchivos=e=>{
        setArchivos(e);
    }

    const insertarArchivos=async()=>{
        const f = new FormData();

        for (let index=0; index < archivos.length; index++){
            f.append("image", archivos[index]);
        }

        API.post("user/image",f)
        .then(response=>{
            image = response.data.results;
            window.location.reload(false);
            // console.log(response.data);
            window.location.reload(false);
            console.log(image);
        }).catch(error=>{
            console.log(error);
        })

        
    }
    // let hola =[];

    const [ userUserDetails, setUserPostDetails ] = useState({});
    // const [ imageOfGuardian,setImageOfGuardian ]= useState()

    const getUserDetails = theUser => {

        API.get('user',theUser).then(res =>{
            
            console.log("CARGA DE RESULTADOS DEL GUARDIAN")
            // hola = res.data.results;
            // console.log(hola)
            setUserPostDetails(res.data.results[0])
            
            // setImageOfGuardian(res.data.results[0].image)
            
           
            })
    }
    // console.log(imageOfGuardian)
    
    // console.log(postDetails[0].image)


    function verifyToken(){
        API.post('init').then(res => {
            document.cookie ='isLogged= true';
            // document.cookie ='loggedAs= user';
            setIsLogged(true)
            }).catch( err =>{
            document.cookie = 'isLogged= false';
            setIsLogged(false)
            history.push('login')
            });
        }
    
    useEffect(verifyToken,[]);

    useEffect(initUserPage,[getCookieUtil('loggedAs')]);
    useEffect(() => getUserDetails(), []);



    // useEffect(getGuardianPostDetails,[hola])
    
    return(
     <div className="userPageContainer">
        <div className="userPage">
            <div className="userPage__user">
            
                <div>
                    <h2 className="userPage__h2">{userUserDetails.name}</h2>
                   
                    <Link to="/edituser" className="userPage__text">Puedes ver y editar tu perfil</Link>
                
                </div>
                    
                    
                <div>
                    <img className="userPage__img" src={userUserDetails.image ? userUserDetails.image : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOAAAADgCAMAAAAt85rTAAAAYFBMVEVmZmb///9VVVVeXl5iYmJaWlpcXFzR0dFWVlZwcHD8/PysrKzf39/GxsZsbGzt7e2Ojo719fXn5+d+fn6VlZWlpaWEhITAwMDU1NS4uLjc3NyLi4udnZ1vb293d3fLy8uj0/C3AAAEW0lEQVR4nO2b2XriMAyFwUvIAgFCWAItff+3HJjIIWUoYwcXLHP+yzbhkyL7yLbk0QgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAv4aSidbihNaJVK+2xjdSy3q7q5bjE8tqt61Pf3i1Tf5Qumyq8RVVU+o44qhEPb32rmVaiwhcTBfz2+6dmS/SV9v3ILL8IXpdFEvWc1Gs77t3Zi1ebeVw9KTnSD4pFpkUQmaLYpL3/jHRr7ZzIEr1Zt9sI7rsd8qIYjPrzUTFUmuUWnbBK3Ry5YNKdNGFccnSw4t/W3lTSKTcXjx8tnWPo834XK5+nGN6ZYI4ZzcPhdGX6fXg7KO0ySITZloq15aGdx9izSsfltaB6Twsn2GXL1IaeVOLgSfMs4xWbWpB+pLYPJ2Q3C74SKkgBV1ZmaxWpKRsdEbVlP8stV9TPqy5hJC0P7fWRdmmwz2XZEgSWtg7WLASUtm05jrEQ7dvNDxyoW7PX2ZWEtqStHuLiscYlW04Ng6SoTbtOywiSBqaO4m+yPnoKG2CJg4j9DRGJ7Sx+i2rPJLsHDX0DOnozumrvAjSGLeFFy3uWKiMbpeWmdtbWbt45eCgGCSIJL0clqNikKnD3noJ7+JgvEM0epGJPk1En+ijX6pFv9iOfrv0Bhve2I8soj90GnxsOOUxQt/g4Df6o/uu+JJbhVBTmZdR8SX68ln8BdD4S9i9JoR7TZN8mxB6bSR5nG0k8TcCxd/KFX0z3ij6dspR/A2xFi3Ne94tzSOVJvt7/k0z1o33SizuundmvmZ7RUSJ+s6Nggt5kbKMol79c9vlRxc/+AmNSr6+O1HNisWxPF/OKo+HYnalPVXGbJzqQz/TLbcbreXlyplSUqer7/eZClZBFNue6bOVuHWd7uRk2fQ+w5TPnTuVXAZgXsg7Tdsy7elQnjHxUJXdKnvcpP+ZWyepvTy9YTERVdYZvFcWFqu06V6oGZTPev7VlsdIyWc3TuvgY6jMcdN4PrKeUirtcsohdA+lmVFfTrKfdtuOY9hKYw45x41jWktqo6VBh9BUGcaF8ymu3Jh8GHDGV4cuO7i/LE0M3doXnkqSD5l/3dtmHjq22DwPTVpYDRxk6YPv/zamk2A8WAdN2e0jTCU15j1QBvskJQ2y0GTqupMHrEuagHXGBPAh29J2nWBXOX0upqr72Mc3w2Ad3izUey/Th8ZBgPV65+aY25gQ2vUvPBFTCnt48qTtYmEWmsyQOrg0qN2GGsHcmhWfwNHfZqf9JZdmxSdA333p4btT2T6wxjwqsfiwir5VYI1rdJfg4GFcmc6uoBz0alQaYKJQHx6HFS0ZglrMUBb0k7zkLLxMSLcC/GwCVIB3KEja/TRE0slOUB3qdF/p6OfX2kVDUPeYaAHpqSOS1u0hbev93v4L8C5h9A6OxF/C/DUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADg7fgDyhYmCCEbhAQAAAAASUVORK5CYII="} alt=""/>
                    
                </div>
            </div>
            <div className="btns">
                <label className="labelPhotosEdit" htmlFor="photosEdit"><img className="svgEdit" src={vector} alt="hola" />Foto de perfil</label>
                <input className="photosEdit" name="photosEdit" id="photosEdit"  type="file" multiple onChange={(e)=>subirArchivos(e.target.files)} />
                <button className="becomeButtonsEdit__upload" onClick={()=>insertarArchivos()}>Subir Foto</button>
            </div>
            <section className="list">
            <div className="list__item">

               { loggedAs == 'user'  &&
                   <div onClick={changeToUserOrGuardian} to="/seleccionaubicacion"><p className="list__p">Conviértete en guardián</p>

                    <span className="list__span">Puedes ganar 400€ de media al mes</span>
                    <hr className="userPage__hr"/>
                </div>}

               { loggedAs == 'guardian'  &&
                   <div onClick={changeToUserOrGuardian} to="/seleccionaubicacion"><p className="list__p">Conviértete en Usuario</p>
                    <hr className="userPage__hr"/>
                </div>}

                </div>
                <div className="list__item">
                    <p className="list__p">Invita a tus amigos</p>
                    <span className="list__span">Y podrás ganar descuentos para ti</span>
                    <hr className="userPage__hr"/>
                </div>
                <div className="list__item">
                    <p className="list__p">Créditos y descuentos</p>
                    <hr className="userPage__hr"/>
                </div>
                <div className="list__item">
                    <Link className="list__p" to="/seleccionaubicacion"><p className="list__p">Publica tu anuncio o experiencia</p></Link>
                    <hr className="userPage__hr"/>
                </div>
                <div className="list__item">
                    <p className="list__p">Configuración</p>
                    <hr className="userPage__hr"/>
                </div>
                <div className="list__item">
                    <p className="list__p">Ayuda</p>
                    <hr className="userPage__hr"/>
                </div>
                
            </section>
            
        </div>

    </div>
    

    )
}