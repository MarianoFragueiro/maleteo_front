import axios from 'axios';
import { getCookieUtil } from "../Utils/getCookieUtil";

// window.localStorage.getItem('token')
// getCookieUtil('token')

export const APIHeaders = {
    // 'Accept': 'application/json',
    'Accept': '*/*',
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    'Authorization': {
        toString () {
            return `Bearer ${getCookieUtil('token')}`
        }
    }
};


export const API = axios.create({
    baseURL: process.env.REACT_APP_BACK_URL,
    timeout: 600000000000,
    headers: APIHeaders
});

