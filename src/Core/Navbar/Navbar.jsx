import React, { useContext, useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import './Navbar.scss';
import logoHome from '../../Assets/Logos/home.png';
import logoMessage from '../../Assets/Logos/message.png';
import logoSearch from '../../Assets/Logos/search.png';
import logoUser from '../../Assets/Logos/user.png';
import guardianAnun from '../../Assets/Logos/anuncio.svg';
import guardianCalendar from '../../Assets/Logos/calendarnav.svg';
import { getCookieUtil } from "../../Shared/Utils/getCookieUtil";
import { IsLoggedContext, loggedAsContext } from "../../Shared/Contexts/IsLoggedContext";


export function Navbar(){

    const { loggedAs, setLoggedAs } = useContext(loggedAsContext);
    const { isLogged, setIsLogged } = useContext(IsLoggedContext);

    

    return(
    <>
        { loggedAs == 'user' && isLogged &&
            <nav className="userNavbar" >
            <NavLink exact to="/userhome"><img src={logoHome} alt="home"/></NavLink>
            {/* <NavLink exact to="/searchGuardian"><img src={logoSearch} alt="buscar"/></NavLink> */}
            <NavLink exact to="/inbox"><img src={logoMessage} alt="mensaje"/></NavLink>
            <NavLink exact to="/userpage"><img src={logoUser} alt="usuario"/></NavLink>
            </nav>
        }

        { loggedAs == 'guardian' && isLogged &&
            <nav className="userNavbar" >
            <NavLink exact to="/inbox"><img src={logoMessage} alt="mensaje"/></NavLink>
            <NavLink exact to="/postguardian"><img src={guardianAnun} alt="anuncio"/></NavLink>
            <NavLink exact to="/bookingrequest"><img src={guardianCalendar} alt="calendar"/></NavLink>
            <NavLink exact to="/userpage"><img src={logoUser} alt="usuario"/></NavLink>
            </nav>
        }
    </>
    )
}