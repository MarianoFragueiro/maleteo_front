import './InboxPage.scss';
import noImage from '../../../Assets/img/No_Image.jpg';
import {  useContext, useEffect, useState } from 'react';
import { API } from '../../../Shared/Services/Api';
import { getCookieUtil } from '../../../Shared/Utils/getCookieUtil';
import { useHistory } from 'react-router';
import { IsLoggedContext, loggedAsContext } from '../../../Shared/Contexts/IsLoggedContext';

export function InboxPage(){
    const { isLogged, setIsLogged } = useContext(IsLoggedContext);
    const { loggedAs, setLoggedAs } = useContext(loggedAsContext);
    const userId = getCookieUtil('userId');
    const [ guardianRequests, setGuardianRequests ] = useState([]);
    const [ allMessages, setAllMessages ] = useState([]);
    const [ requestDone, setRequestDone ] = useState(false);
    const history = useHistory();
    
    let sortedMessages = []
    // let AllMessages = {}


    const getGuardianPostDetails = () => {

        API.get('guardianmessages').then(res =>{
            console.log("CARGA DE RESULTADOS")
            console.log(res.data.results)
            setGuardianRequests(res.data.results)
            setRequestDone(true)
            
            SortMessages()
            })

    }

    function SortMessages(){
        console.log('Sorting messages');
        let lastMessage =[]
        console.log(guardianRequests);
        guardianRequests.map(guardianRequest => {

        if(guardianRequest.userMessages){
            let userMessages = guardianRequest.userMessages
            // console.log(userMessages);
            for (let messages of userMessages){
                const value = messages.split(";;")
                const dataMess = {date:value[0],userMessage:value[1]}
                sortedMessages.push(dataMess)
                console.log('user sort: ',dataMess);
            }
        }

        if(guardianRequest.guardianMessages){
            let guardianMessages = guardianRequest.guardianMessages
            // console.log(guardianMessages);
            for (let messages of guardianMessages){
                const value = messages.split(";;")
                const dataMess = {date:value[0],guardianMessage:value[1]}
                sortedMessages.push(dataMess)
                console.log('guardian sort: ',dataMess);
            }
        }

        sortedMessages = sortedMessages.sort((a, b) => new Date(b.date) - new Date(a.date) )
        console.log('Sorted messages:',sortedMessages);
        if(sortedMessages[0]){
            if(sortedMessages[0].userMessage){lastMessage.push(sortedMessages[0].userMessage)}
            if(sortedMessages[0].guardianMessage){lastMessage.push(sortedMessages[0].guardianMessage)}
        }
        // lastMessage.push(sortedMessages[0].messages)
        console.log('Last Message',lastMessage);
        sortedMessages =[]
        return
        })
        setAllMessages( lastMessage )
        console.log('All Messages', allMessages);
        console.log('algo');
        setRequestDone(true)
    }

    function goToMessage(message_id){
        history.push('/messagespage/'+message_id)
    }
    
    function verifyToken(){
        API.post('init').then(res => {
            document.cookie ='isLogged= true';
            // document.cookie ='loggedAs= user';
            setIsLogged(true)
            }).catch( err =>{
            document.cookie = 'isLogged= false';
            setIsLogged(false)
        });
        // if(getCookieUtil('sLogged')==false){
        //     history.push('login')
        // }
        }
    
        
        
        
    useEffect(verifyToken,[]);
    useEffect(getGuardianPostDetails, [requestDone]);
    // useEffect(SortMessages, [requestDone]);
    
    return(
        <div className="myCenter">
        <div className="myInbox">
            <h1 className="myInbox__title">Mensajes</h1>
            <div className="map">
            {requestDone && guardianRequests&&
            <>
                {guardianRequests.map((messages,i) =>
                <>
                <div className="myInbox__user">
                    {/* <img className="myInbox__img" src={messages.user.image  ? messages.user.image : noImage } alt=""/> */}
                    { userId != messages.user._id &&
                        <div className="c-chat__image col-3">
                                <img className="c-chat__img" src={messages.user.image  ? messages.user.image : noImage } alt=""/>

                        </div>}
                        { userId != messages.guardian._id &&
                        <div className="c-chat__image col-3">
                                <img className="c-chat__img" src={messages.guardian.image  ? messages.guardian.image : noImage } alt=""/>

                        </div>}
                    <section className="myInbox__info">
                        <div onClick={()=>goToMessage(messages._id)} className="myInbox__name">
                            <h3 className="myInbox__h3">
                            {userId == messages.user._id && <span > {messages.guardian.name} </span> }
                            {userId == messages.guardian._id && <span > {messages.user.name} </span> }
                            </h3>
                            <span className="myInbox__span">{messages.status}</span>
                        </div>
                        <p className="myInbox__time">{messages.date}</p>
                        <p className="myInbox__text">
                        {allMessages[i]}
                        
                        </p>
                    </section>
                </div>
                    <hr className="myInbox__hr"/>
            </>
            )}
            </>}
            </div>
            
        </div>
        
        </div>
    )
}


