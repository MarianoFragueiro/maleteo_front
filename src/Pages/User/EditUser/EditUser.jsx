import axios from 'axios';
import { useContext, useEffect, useState } from 'react';
import './EditUser.scss';
import logoArrow from '../../../Assets/Logos/arrow.png';
import { Link, useHistory } from 'react-router-dom';
import { getCookieUtil } from '../../../Shared/Utils/getCookieUtil';
import { API } from '../../../Shared/Services/Api';
import { useForm } from 'react-hook-form';
import { IsLoggedContext, loggedAsContext } from '../../../Shared/Contexts/IsLoggedContext';



export function EditUser(){
    const history = useHistory();
    const { register, handleSubmit } = useForm();
    const [ user, setUsers ] = useState([]);
    const { isLogged, setIsLogged } = useContext(IsLoggedContext);
    const { loggedAs, setLoggedAs } = useContext(loggedAsContext);

    const getUserDetails = () => {
        const userId =  getCookieUtil('user');
        API.get('user/'+userId).then(res => {
            setUsers(res.data.results[0]);
            console.log(res.data.results[0]);
        }).catch( err =>{
            console.log(err.response);
        });
        // UpdateDatas()
    }

    function UpdateDatas(formData){
        const userId =  getCookieUtil('userId');

        const dataToUpdate ={}
        const name = formData.name ? dataToUpdate.name = formData.name : false; //Corroboro si existe el dato
        const surname = formData.surname ? dataToUpdate.surname = formData.surname : false;
        const birthDate = formData.birthDate ? dataToUpdate.birthDate = formData.birthDate :false;
        const password = formData.password ? dataToUpdate.password = formData.password :false;

        API.patch('user/'+userId, dataToUpdate).then(res => {
            console.log(res.data.results);
            history.push('/userpage')
        }).catch( err =>{
            console.log('Error');
            console.log(err);
        });
    }

    function verifyToken(){
        API.post('init').then(res => {
            document.cookie ='isLogged= true';
            // document.cookie ='loggedAs= user';
            setIsLogged(true)
            }).catch( err =>{
            document.cookie = 'isLogged= false';
            setIsLogged(false)
            history.push('login')
            });
        }
    
    useEffect(verifyToken,[]);


    useEffect(getUserDetails, []);

    return(
    <div className="editUserContainer">
    <div className="mainUser">
    <Link to="/userpage"><img className="mainUser__img" src={logoArrow} alt="arrow"/></Link>
        <form className="editForm" onSubmit={handleSubmit(UpdateDatas)} >
                
                <label className="editForm__label" htmlFor="email">Dirección de correo electrónico</label>
                <input className="editForm__input" name="email" id="email" defaultValue={user.email ? user.email  : ""} placeholder="ejemplo@gmail.com" ref={register()}/>
                
                <label className="editForm__label" htmlFor="password">Contraseña</label>
                <input className="editForm__input" name="password" placeholder="********" id="password" type="password" ref={register()}/>

                <label className="editForm__label" htmlFor="name">nombre</label>
                <input className="editForm__input" name="name" id="name" type="name" defaultValue={user.name ? user.name : ""} placeholder="Nombre" ref={register()}/>

                <label className="editForm__label" htmlFor="surname">Apellido</label>
                <input className="editForm__input" name="surname" id="surname" type="surname" defaultValue={user.surname? user.surname : ""} placeholder="Apellido" ref={register()}/>

                <label className="editForm__label" htmlFor="birthDate">Fecha de nacimiento</label>
                <input className="editForm__input" type="date" name="birthDate" id="birthDate" defaultValue={user.date? user.date : "2018-07-22"}  ref={register()} /> 
                
                <input className="editForm__btn" type="submit" value="Guardar"/>
                
               
            </form>
    </div>
    </div>

    )
}