import { useContext, useEffect } from "react";
import { useHistory } from "react-router";
import { GoogleMap } from "../../../Shared/GoogleMap/GoogleMap";
import { API } from "../../../Shared/Services/Api";
import './SearchGuardian.scss';
import arrowBack from '../../../Assets/Logos/leftsolid.svg';
import { Link } from "react-router-dom";
import { IsLoggedContext, loggedAsContext } from "../../../Shared/Contexts/IsLoggedContext";
 



export function SearchGuardian(){

    const { isLogged, setIsLogged } = useContext(IsLoggedContext);
    const { loggedAs, setLoggedAs } = useContext(loggedAsContext);
    const history = useHistory();

    function verifyToken(){
        API.post('init').then(res => {
            document.cookie ='isLogged= true';
            // document.cookie ='loggedAs= user';
            setIsLogged(true)
            }).catch( err =>{
            document.cookie = 'isLogged= false';
            setIsLogged(false)
            history.push('login')
            });
        }
    
    useEffect(verifyToken,[]);

    return(
     <div>
        <div className="searchGuardian">
            <Link to='./userhome'>
                <img  className="searchGuardian__img"  src={arrowBack} alt=""/>
            </Link>
            <p className="searchGuardian__p">Bienvenido a la busqueda de Guardianes</p>
            {/* <div className="">
                <input id="pac-input" class="controls" type="text" placeholder="Search Box"/>
            </div> */}
        </div>
        <GoogleMap/>

    </div>

    )
}