import axios from "axios";
import { useContext, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import logoArrow from '../../../Assets/Logos/arrow.png';
import logoPlus from '../../../Assets/Logos/plus-solid.svg';
import { API } from "../../../Shared/Services/Api";
import { useHistory } from "react-router";
import { getCookieUtil } from "../../../Shared/Utils/getCookieUtil";
import './PostGuardian.scss';


import image22 from '../../../Assets/img/img2.png';
import { Navbar } from "../../../Core/Navbar/Navbar";
import { IsLoggedContext, loggedAsContext } from "../../../Shared/Contexts/IsLoggedContext";


export function PostGuardian(){
    const history = useHistory();
    
    const [ anuncio, setAnuncio ] = useState([]);
    const { isLogged, setIsLogged } = useContext(IsLoggedContext);
    const { loggedAs, setLoggedAs } = useContext(loggedAsContext);

    let myPostGuardian =[];

    const getGuardianPost = () => {
     
        const userId = getCookieUtil("userId")

        API.get('postsuser').then(res =>{
            myPostGuardian = res.data.results
            console.log("CARGA DE RESULTADOS")
            console.log(myPostGuardian)
            setAnuncio(myPostGuardian)
            })
    }
    

    function verifyToken(){
        API.post('init').then(res => {
            document.cookie ='isLogged= true';
            // document.cookie ='loggedAs= user';
            setIsLogged(true)
            }).catch( err =>{
            document.cookie = 'isLogged= false';
            setIsLogged(false)
            history.push('login')
            });
        }
    
    useEffect(verifyToken,[]);

    useEffect(() => getGuardianPost(), []);

    return(
    <div className="postGuardian">
        <div className="postGuardian__div">
            <Link to="/userpage"><img className="postGuardian__logoA" src={logoArrow} alt="arrow"/></Link>
            <div className="postForm" >

                <h1 className="postGuardian__h1">Tus anuncios</h1>

                {anuncio.map((item,i)=>
                <div className="postGuardian__post" key={i}>
                    <h2 className="postGuardian__h2">{item.title}</h2>
                    <p className="postGuardian__p">Anuncio completo al 100%</p> 
                    <img className="postGuardian__img"  src={item.images[0] ? item.images[0] : "https://ecoinventos.com/wp-content/uploads/2016/08/Casa-bioclimatica1.jpg"} alt=""/>
                    <div className="postGuardian__toRight">
                        <Link to="/userpage"><img className="postGuardian__arrow" src={logoArrow} alt="arrow"/></Link>
                    </div>
                    <hr className="myInbox__hr"/>
                </div>)}

                

                <div>
                    <h2 className="postGuardian__h2">Añade otro espacio</h2>
                    {/* añadir el simbolo + en vez la flecha */}
                    <div className="postGuardian__toRight">
                        <Link to="/seleccionaubicacion"><img className="postGuardian__plus" src={logoPlus} alt="arrow"/></Link> 
                    </div>
                </div>

                <hr className="myInbox__hr"/>
            
            </div>
        </div>
        <Navbar/>
    </div>
    
    )
}
